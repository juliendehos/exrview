with import <nixpkgs> {}; 

let
  myopencv = opencv4.override {
    enableGtk3 = true;
  };

in

stdenv.mkDerivation {
    name = "exrview";
    buildInputs = [ stdenv pkgconfig cmake myopencv ];
    src = ./.;
}

