# exrview
[![Build status](https://gitlab.com/juliendehos/exrview/badges/master/build.svg)](https://gitlab.com/juliendehos/exrview/pipelines) 

## description

display a high-dynamic range image using basic tone-mapping (x' = coef * x^gamma)

![](doc/screenshot1.png)


## build & run

install OpenCV with OpenEXR support then


```
$ mkdir build
$ cd build
$ cmake ..
$ make
$ ./exrview ../data/test2.exr 
filename: ../data/test2.exr
cols: 512
rows: 512
channels: 3
depth: CV_32F
min: 0
max: 15.6953
mean: [0.000389033, 0.00125488, 0.00331183, 0]
```

