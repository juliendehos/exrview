#include <opencv2/opencv.hpp>
#include <iostream>
#include <string>

using namespace std;

void printImageStats(const cv::Mat & img, const string & filename) {

    cout << "filename: " << filename << endl;
    cout << "cols: " << img.cols << endl;
    cout << "rows: " << img.rows << endl;
    cout << "channels: " << img.channels() << endl;

    cout << "depth: ";
    switch (img.depth()) {
        case CV_8U: cout << "CV_8U" << endl; break;
        case CV_8S: cout << "CV_8S" << endl; break;
        case CV_16U: cout << "CV_16U" << endl; break;
        case CV_16S: cout << "CV_16S" << endl; break;
        case CV_32S: cout << "CV_32S" << endl; break;
        case CV_32F: cout << "CV_32F" << endl; break;
        case CV_64F: cout << "CV_64F" << endl; break;
        default: cout << "unknown format" << endl;
    }

    double minVal, maxVal; 
    cv::minMaxIdx(img, &minVal, &maxVal);
    cout << "min: " << minVal << endl;
    cout << "max: " << maxVal << endl;

    cv::Scalar meanScalar = cv::mean(img);
    cout << "mean: " << meanScalar << endl;
}

cv::Mat loadImg(const string & filename) {

    cv::Mat inputImage = cv::imread(filename, 
            cv::IMREAD_ANYDEPTH | cv::IMREAD_ANYCOLOR);

    if (inputImage.empty()) {
        cerr << "error: unable to open " << filename << endl;
        exit(-1);
    }

    return inputImage;
}

struct Data {
    cv::Mat src;
    cv::Mat dst;
    string winName;
    int coef, gamma;
};

void handleTrackbar(int, void * d) {
    Data * data = (Data *) d;
    cv::normalize(data->src, data->dst, 0, 1, cv::NORM_MINMAX);
    cv::pow(data->dst, data->gamma/100.0, data->dst);
    data->dst *= 255.0 * data->coef/100.0;
    imshow(data->winName, data->dst);
}

// prevent errors in console output
int handleError(int, const char *, const char *, const char *, int, void *) {
    return 0;
}

int main(int argc, char ** argv) {

    if (argc != 2) {
        cerr << "usage: " << argv[0] << " <filename>\n";
        exit(-1);
    }
    const string FILENAME = argv[1];

    cv::redirectError(handleError);

    Data data;
    data.src = loadImg(FILENAME);
    data.dst = data.src.clone();
    data.winName = "exrview: " + FILENAME;
    data.coef = 100;
    data.gamma = 100;

    printImageStats(data.src, FILENAME);
    cv::namedWindow(data.winName, cv::WINDOW_AUTOSIZE);
    cv::createTrackbar("coef (%)", data.winName, &data.coef, 500, handleTrackbar, &data);
    cv::createTrackbar("gamma (%)", data.winName, &data.gamma, 500, handleTrackbar, &data);
    handleTrackbar(0, &data);

    try {
        // terminate when window is closed
        while (cv::getWindowProperty(data.winName, 0) != -1) {
            // terminate when <esc> is hit
            if (cv::waitKey(30) % 0x100 == 27)
                cv::destroyAllWindows();
        }
    }
    catch (cv::Exception&) {
    }

    return 0;
}

